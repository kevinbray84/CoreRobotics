//=====================================================================
/*
Software License Agreement (BSD-3-Clause License)
Copyright (c) 2017, CoreRobotics.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

* Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

* Neither the name of CoreRobotics nor the names of its contributors
may be used to endorse or promote products derived from this
software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

\project CoreRobotics Project
\url     www.corerobotics.org
\author  Parker Owan, Tony Piaskowy

*/
//=====================================================================

#include <iostream>
#include "CoreRobotics.hpp"
#include "gtest/gtest.h"


// Use the CoreRobotics namespace
using namespace CoreRobotics;


//
// constructor
//
TEST(CRFrameEuler, Construct){
    CRFrameEuler frame;
    Eigen::VectorXd p = frame.getPose(CR_EULER_MODE_XYZ);
    EXPECT_DOUBLE_EQ(0, p(0));
    EXPECT_DOUBLE_EQ(0, p(1));
    EXPECT_DOUBLE_EQ(0, p(2));
    EXPECT_DOUBLE_EQ(0, p(3));
    EXPECT_DOUBLE_EQ(0, p(4));
    EXPECT_DOUBLE_EQ(0, p(5));
}


//
// SetFreeValue/GetFreeValue
//
TEST(CRFrameEuler, GetFreeValue){
    CRFrameEuler frame(0, 0, 0, M_PI / 2, -M_PI / 2, M_PI / 4, CR_EULER_MODE_XYZ, CR_EULER_FREE_NONE);
    EXPECT_EQ(NULL, frame.getFreeValue());
    
    frame.setFreeVariable(CR_EULER_FREE_ANG_A);
    EXPECT_DOUBLE_EQ(M_PI / 2, frame.getFreeValue());
    
    frame.setFreeValue(0);
    EXPECT_DOUBLE_EQ(0, frame.getFreeValue());
}


//
// SetFreeVariable/GetFreeVariable
//
TEST(CRFrameEuler, GetFreeVariable){
    CRFrameEuler frame(0, 0, 0, M_PI / 2, -M_PI / 2, M_PI / 4, CR_EULER_MODE_XYZ, CR_EULER_FREE_NONE);
    EXPECT_EQ(CR_EULER_FREE_NONE, frame.getFreeVariable());
    EXPECT_FALSE(frame.isDriven());
    
    frame.setFreeVariable(CR_EULER_FREE_ANG_A);
    EXPECT_EQ(CR_EULER_FREE_ANG_A, frame.getFreeVariable());
    EXPECT_TRUE(frame.isDriven());
}


//
// SetMode/GetMode
//
TEST(CRFrameEuler, GetMode){
    CRFrameEuler frame(0, 0, 0, M_PI / 2, -M_PI / 2, M_PI / 4, CR_EULER_MODE_XYZ, CR_EULER_FREE_NONE);
    EXPECT_EQ(CR_EULER_MODE_XYZ, frame.getMode());
    
    frame.setMode(CR_EULER_MODE_ZYX);
    EXPECT_EQ(CR_EULER_MODE_ZYX, frame.getMode());
}


//
// SetPositionAndOrientation/GetPositionAndOrientation
//
TEST(CRFrameEuler, GetPositionAndOrientation){
    CRFrameEuler frame(0, 0, 0, M_PI / 2, -M_PI / 2, M_PI / 4, CR_EULER_MODE_XYZ, CR_EULER_FREE_NONE);
    double x, y, z, a, b, g;
    frame.getPosition(x, y, z);
    EXPECT_DOUBLE_EQ(0, x);
    EXPECT_DOUBLE_EQ(0, y);
    EXPECT_DOUBLE_EQ(0, z);
    
    frame.setPosition(0.1, 0.2, 0.3);
    frame.getPosition(x, y, z);
    EXPECT_DOUBLE_EQ(0.1, x);
    EXPECT_DOUBLE_EQ(0.2, y);
    EXPECT_DOUBLE_EQ(0.3, z);
    
    frame.getOrientation(a, b, g);
    EXPECT_DOUBLE_EQ( M_PI / 2, a);
    EXPECT_DOUBLE_EQ(-M_PI / 2, b);
    EXPECT_DOUBLE_EQ( M_PI / 4, g);
    
    frame.setOrientation(0.1, 0.2, 0.3);
    frame.getOrientation(a, b, g);
    EXPECT_DOUBLE_EQ(0.1, a);
    EXPECT_DOUBLE_EQ(0.2, b);
    EXPECT_DOUBLE_EQ(0.3, g);
    
    frame.setPositionAndOrientation(0, 0, 0, M_PI / 2, -M_PI / 2, M_PI / 4);
    frame.getPositionAndOrientation(x, y, z, a, b, g);
    EXPECT_DOUBLE_EQ(0, x);
    EXPECT_DOUBLE_EQ(0, y);
    EXPECT_DOUBLE_EQ(0, z);
    EXPECT_DOUBLE_EQ( M_PI / 2, a);
    EXPECT_DOUBLE_EQ(-M_PI / 2, b);
    EXPECT_DOUBLE_EQ( M_PI / 4, g);
}
